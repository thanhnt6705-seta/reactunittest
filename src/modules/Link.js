import React from 'react'

const Link = (props) => (
    <a href={props.page || '#'}>
        {props.children}
    </a>
)

export default Link