import fetch from 'cross-fetch'

const url = 'https://randomuser.me/api/?results=5'

export function fetchUser(callback) {
    fetch(url)
        .then(response => response.json())
        .then(data => callback(data.results))
}

export function fetchUserPromise() {
    return fetch(url)
        .then(response => response.json())
        .then(data => data.results)
}