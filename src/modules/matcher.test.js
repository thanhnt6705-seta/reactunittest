

describe('Matchers tests', () => {
    test('2 + 2 = 4', () => {
        expect(2 + 2).toBe(4)
    })

    test('2 * 5 > 9', () => {
        expect(2 * 5).toBeGreaterThan(9)
    })

    test('object assignment', () => {
        const data = {one: 1}
        data['two'] = 2;
        expect(data).toEqual({one: 1, two: 2})
    })

    test('adding positive number is not zero', () => {
        for (let a = 1; a < 10; a++) {
            for (let b = 1; b < 10; b++) {
                expect(a + b).not.toBe(0)
            }
        }
    })
})

describe('Truthiness', () => {
    test('null', () => {
        const n = null
        expect(n).toBeNull()
        expect(n).toBeDefined()
        expect(n).not.toBeUndefined()
        expect(n).not.toBeTruthy()
        expect(n).toBeFalsy()
    })

    test('zero', () => {
        const z = 0
        expect(z).not.toBeNull()
        expect(z).toBeDefined()
        expect(z).not.toBeUndefined()
        expect(z).not.toBeTruthy()
        expect(z).toBeFalsy()
    })
})

describe('Number tests', () => {
    test('integer number', () => {
        const value = 2 + 2
        expect(value).toBeGreaterThan(3)
        expect(value).toBeGreaterThanOrEqual(3.5)
        expect(value).toBeLessThan(5)
        expect(value).toBeLessThanOrEqual(4.5)

        expect(value).toBe(4)
        expect(value).toEqual(4)
    })

    test('adding floating point numbers', () => {
        const value = 0.1 + 0.2
        expect(value).toBeCloseTo(0.3)
    })
})

describe('String tests', () => {
    test('not match string', () => {
        expect('team').not.toMatch('/I/')
    })

    test('match string', () => {
        expect('Christoph').toMatch(/stop/)
    })
})

describe('Array tests', () => {
    const shoppingList = [
        'diapers',
        'kleenex',
        'trash bags',
        'paper towels',
        'beer',
    ]
    test('array has beer', () => {
        expect(shoppingList).toContain('beer')
    })

    test('array has array', () => {
        expect(shoppingList).not.toContain('paper')
    })
})

describe('Exception test', () => {
    function compileAndroidCode() {
        throw new Error('you are using the wrong JDK')
    }

    test('compiling android goes as expected', () => {
        expect(compileAndroidCode).toThrow()
        expect(compileAndroidCode).toThrow(Error)
        expect(compileAndroidCode).toThrow('you are using the wrong JDK')
        expect(compileAndroidCode).toThrow(/JDK/)
    })
})