import { timerGame, infiniteTimerGame } from "./TimerMock";

'./TimerMock'

describe('Timer mock', () => {
    describe('Timer game', () => {
        it('wait 1 second before ending the game', () => {
            jest.useFakeTimers()
            timerGame()

            expect(setTimeout).toHaveBeenCalledTimes(1)
            expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 1000)

            expect(setTimeout.mock.calls.length).toBe(1)
            expect(setTimeout.mock.calls[0][1]).toBe(1000)

            jest.clearAllTimers()
        })

        it('calls the callback after 1 second', () => {
            jest.useFakeTimers()
            const callback = jest.fn()

            timerGame(callback)

            expect(callback).not.toBeCalled()

            jest.runAllTimers()

            expect(callback).toBeCalled()
            expect(callback).toHaveBeenCalledTimes(1)

            jest.clearAllTimers()
        })

        // it('calls the callback after 1 second via advanceTimersByTime', () => {
        //     jest.useFakeTimers()
        //     const callback = jest.fn()
        //
        //     timerGame(callback)
        //
        //     expect(callback).not.toBeCalled()
        //
        //     jest.advanceTimersByTime(1000);
        //
        //     expect(callback).toBeCalled()
        //     expect(callback).toHaveBeenCalledTimes(1)
        //
        //     jest.clearAllTimers()
        // })
    })

    describe('Infinite game', () => {
        it('schedules a 10-seconds timer after 1 second', () => {
            jest.useFakeTimers()
            const callback = jest.fn()

            infiniteTimerGame(callback)

            expect(setTimeout).toHaveBeenCalledTimes(1)
            expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 1000)

            jest.runOnlyPendingTimers()

            expect(callback).toBeCalled()

            expect(setTimeout).toHaveBeenCalledTimes(2)
            expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 10000)

            jest.clearAllTimers()
        })
    })
})