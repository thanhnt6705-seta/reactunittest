import React from 'react'
import PropTypes from 'prop-types'

class SnapshotEnzyme extends React.Component {

    state = {
        clicked: false
    }

    _handleClick = () => {
        alert("clicked!")
        this.setState({clicked: true})
    }

    render() {
        const {url, title} = this.props

        return <a href={url}
                  onClick={this._handleClick}>title</a>
    }
}

SnapshotEnzyme.propTypes = {
    url: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired
}

export default SnapshotEnzyme