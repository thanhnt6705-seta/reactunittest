import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';

import LoadingIndicator from './modules/LoadingIndicator'

class App extends Component {

    state = {
        isLoading: true
    }

    componentDidMount() {
        this._timer = setTimeout(
            () => this.setState({isLoading: false}),
            2000
        )
    }

    componentWillUnmount() {
        clearTimeout(this._timer)
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">Welcome to React</h1>
                </header>
                <p className="App-intro">
                    To get started, edit <code>src/App.js</code> and save to reload.
                </p>

                <LoadingIndicator isLoading={this.state.isLoading}>
                    <div>ahoy!</div>
                </LoadingIndicator>
            </div>
        );
    }
}

export default App;
