import {fetchUser, fetchUserPromise} from './asynchronous'

describe('Asynchronous tests', () => {
    test('callback test', done => {
        function callback(data) {
            expect(data.length).toBe(5)
            done()
        }

        fetchUser(callback)
    })

    test('promise test', () => {
        expect.assertions(1)
        return fetchUserPromise().then(data => {
            expect(data.length).toBe(5)
        })
    })

    test('promise resolves', () => {
        expect.assertions(1)
        return expect(fetchUserPromise()).resolves.not.toBeNull()
    })

    // test('promise catch', async () => {
    //     expect.assertions(1)
    //     return expect(fetchUserPromise()).catch(e =>
    //         expect(e).toEqual({
    //             error: 'error'
    //         }))
    // })

    test('async test', async () => {
        expect.assertions(1)
        const data = await fetchUserPromise()
        expect(data.length).toBe(5)
    })

    test('async resolves', async () => {
        expect.assertions(1)
        await expect(fetchUserPromise()).resolves.not.toBeNull()
    })
})