import React from 'react'
import { shallow } from 'enzyme'
import { shallowToJson } from 'enzyme-to-json'
import SnapshotEnzyme from './SnapshotEnzyme'

describe('SnapshotEnzyme', () => {
    it('should render correctly', () => {
        const output = shallow(
            <SnapshotEnzyme url='mockUrl' title='mockTitle'/>
        )
        expect(shallowToJson(output)).toMatchSnapshot()
    })

    it('should handle click event', () => {
        window.alert = jest.fn()
        const output = shallow(
            <SnapshotEnzyme url='mockUrl' title='mockTitle'/>
        )
        output.simulate('click')
        expect(window.alert).toHaveBeenCalledWith('clicked!')
    })

    it('should handle state changes', () => {
        const output = shallow(
            <SnapshotEnzyme url='mockUrl' title='mockTitle'/>
        )
        expect(output.state().clicked).toBeFalsy()
        output.simulate('click')
        expect(output.state().clicked).toBeTruthy()
    })
})